<?php

use App\Bodega;
use App\Medicamento;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class MedicamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,500) as $index) {
           $medicamento = Medicamento::create([
                'nombre' => $faker->sentence($nbWords = 2, $variableNbWords = true),
                'descripcion' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'posologia' => $faker->sentence($nbWords = 6, $variableNbWords = true)
            ]);

           for ($i = 0; $i < 4;$i++ )
           {
               $bodega = Bodega::find(random_int(1, 5));
               $medicamento->bodegas()->attach($bodega, ['inventario' => random_int(0, 100)]);
           }
        }
    }
}
