<?php

use App\Bodega;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BodegasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,5) as $index) {
            Bodega::create([
                'nombre' => $faker->sentence($nbWords = 2, $variableNbWords = true),
                'direccion' => $faker->address,
                'user_id' => random_int(1, 50)
            ]);
        }
    }
}
