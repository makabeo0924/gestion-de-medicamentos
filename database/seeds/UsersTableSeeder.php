<?php

use App\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        User::create([
            'name' => 'Aquiles Estoirico',
            'cedula' => '93120904980',
            'password' => bcrypt('secret'),
        ]);
        foreach (range(1,50) as $index) {
            User::create([
                'name' => $faker->name,
                'cedula' => $faker->isbn10,
                'password' => bcrypt('secret'),
            ]);
        }
    }
}
