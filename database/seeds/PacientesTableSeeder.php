<?php

use App\Medicamento;
use App\Paciente;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PacientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,100) as $index) {
            $paciente = Paciente::create([
                'nombre' => $faker->name,
                'residencia' => $faker->address,
                'cedula' => $faker->isbn10
            ]);

            for ($i = 0; $i < 4;$i++ )
            {
                $medicamento = Medicamento::find(random_int(1, 2801));
                $paciente->medicamentos()->attach($medicamento, ['cantidad' => random_int(0, 100)]);
            }
        }
    }
}
