> ## GESTION DE MEDICAMENTOS (Prueba Tecnica para desarrollador junior Grupo Magin SAS)

Este repositorio es completamente funcional y se encuentra desplegado en heroku en este [link](https://medicamentosmagin.herokuapp.com/) utilizando codeship como herramienta de automatizacion de despliegue

Pudes ver el estado del Despliegue --> [![Codeship Status for makabeo0924/gestion-de-medicamentos](https://app.codeship.com/projects/be764b30-acd0-0137-49dc-0ef830f7bc79/status?branch=master)](https://app.codeship.com/projects/362335)

----------

# Getting started

Este repositorio fue creado utilizando [laravel](https://laravel.com/) en su version 5.8, [Adminlte2](https://adminlte.io/) como template para el backofice y [drcare](https://colorlib.com/wp/template/drcare/) como twmplate para la landing page

## Installation

Clonar el repositorio

    git clone https://makabeo0924@bitbucket.org/makabeo0924/gestion-de-medicamentos.git

Cambiar a la carpeta del repositorio

    cd gestion-de-medicamentos

Instalar todas las dependencias utilizando composer

    composer install

Copiar el ejemplo de .env y realizar la configuracion necesaria para la base de datos

    cp .env.example .env

Generar la llave de la aplicacion

    php artisan key:generate

Correr la migracion de la base de datos (**Ajustar la conexion de la base de datos en el archivo .env antes utilizar este comando**)

    php artisan migrate 

Iniciar el servidor local

    php artisan serve

Se puede acceder al servidor local en http://localhost:8000

**TL;DR lista de comandos**

    git clone https://makabeo0924@bitbucket.org/makabeo0924/gestion-de-medicamentos.git
    cd gestion-de-medicamentos
    composer install
    cp .env.example .env
    php artisan key:generate 
    
**Este seguro de ajustar los datos de la conexion de la base de datos antes iniciar estos comandos** [Variables de entorno](#environment-variables)

    php artisan migrate
    php artisan serve

## Database seeding

**Cargar la base de datos con valores para poder probar la aplicaion con datos ya incluidos y hacer uso del front-end de la misma**

Iniciar el database seeder y listo

    php artisan db:seed

***Note*** : Es recomendable limpiar la base de datos para evitar cualquier error usando este comando

    php artisan migrate:refresh


----------

# Vista general del codigo

## Carpetas

- `app` - Contiene todos los Eloquent models
- `app/Http/Controllers` - Contiene todos los controllers
- `config` - Contiene todos los archivos de configuracion
- `database/migrations` - Contiene todas las migraciones
- `database/seeds` - Contiene todos los database seeders
- `routes` - Contiene todas las rutas en el archivo web.php
- `resources/viewa` - contiene todas las vistas utilizadas
- `public/adminlte` - contiene todos los elementos para el template AdminLte2 utlizado en el backoffice
- `public/drcare` - contiene todos los elementos paa el template drcare utilizado en la landig page

## Environment variables

- `.env` - Las variables de entorno pueden ser organizadas en este archivo

----------

# Authentication
 
Esta aplicacion utiliza el medio de autentificacion por defecto de laravel las credenciales de acceso son

- cedula: 93120904980
- contraseña: secret
