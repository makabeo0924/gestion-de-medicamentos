<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'medicamentos';

    protected $fillable = [
        'nombre', 'descripcion', 'posologia',
    ];

    /**
     * The roles that belong to the user.
     */
    public function bodegas()
    {
        return $this->belongsToMany(Bodega::class, 'bodegas_medicamentos','medicamento_id','bodega_id')
            ->withPivot('inventario');
    }
    /**
     * The roles that belong to the user.
     */
    public function pacientes()
    {
        return $this->belongsToMany(Paciente::class, 'medicamentos_pacientes','medicamento_id','paciente_id')
            ->withPivot('cantidad');
    }
}
