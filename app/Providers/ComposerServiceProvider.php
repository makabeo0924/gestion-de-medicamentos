<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composers([
            'App\Http\View\Composers\UserComposer' => 'users.index',
            'App\Http\View\Composers\BodegaComposer' => 'bodegas.index',
            'App\Http\View\Composers\BodegaComposer@composeCreate' => ['bodegas.create', 'bodegas.edit'],
            'App\Http\View\Composers\PacienteComposer' => 'pacientes.index',
            'App\Http\View\Composers\MedicamentoComposer' => 'medicamentos.index'
        ]);
    }
}
