<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bodega extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bodegas';

    protected $fillable = [
        'direccion', 'nombre',
    ];

    /**
     * Get the post that owns the comment.
     */
    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * The roles that belong to the user.
     */
    public function medicamentos()
    {
        return $this->belongsToMany(Medicamento::class, 'bodegas_medicamentos','bodega_id','medicamento_id')
            ->withPivot('inventario');
    }
}
