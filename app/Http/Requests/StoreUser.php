<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = null;
        if (isset($this->usuario)){
            $user =  $this->usuario->id;
        }
        return [
            'name' => ['required', 'string', 'max:255'],
            'password' => ['required','sometimes','string', 'min:6', 'confirmed'],
            'cedula' => ['required', 'string', 'max:255', Rule::unique('users','cedula')->ignore($user) ],
        ];
    }
}
