<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePaciente extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $paciente = null;
        if (isset($this->paciente)){
            $paciente =  $this->paciente->id;
        }
        return [
            'nombre' => ['required', 'string', 'max:255'],
            'residencia' => ['required', 'string', 'max:255'],
            'cedula' => ['required', 'string', 'max:255', Rule::unique('pacientes','cedula')->ignore($paciente) ],
        ];
    }
}
