<?php


namespace App\Http\View\Composers;


use App\Medicamento;
use Illuminate\View\View;

class MedicamentoComposer
{
    /**
     * The user repository implementation.
     *
     * @var User
     */
    protected $medicamentos;

    /**
     * Create a new profile composer.
     *
     * @param Medicamento $medicamentos
     */
    public function __construct(Medicamento $medicamentos)
    {
        // Dependencies automatically resolved by service container...
        $this->medicamentos = $medicamentos->all();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(['medicamentos' => $this->medicamentos]);
    }
}
