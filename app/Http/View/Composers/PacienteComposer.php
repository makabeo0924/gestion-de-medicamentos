<?php


namespace App\Http\View\Composers;


use App\Paciente;
use Illuminate\View\View;

class PacienteComposer
{
    /**
     * The user repository implementation.
     *
     * @var User
     */
    protected $pacientes;

    /**
     * Create a new profile composer.
     *
     * @param Paciente $pacientes
     */
    public function __construct(Paciente $pacientes)
    {
        // Dependencies automatically resolved by service container...
        $this->pacientes = $pacientes->all();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(['pacientes' => $this->pacientes]);
    }
}
