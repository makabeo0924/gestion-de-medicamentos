<?php


namespace App\Http\View\Composers;


use App\User;
use Illuminate\View\View;

class UserComposer
{
    /**
     * The user repository implementation.
     *
     * @var User
     */
    protected $usuarios;

    /**
     * Create a new profile composer.
     *
     * @param User $usuarios
     */
    public function __construct(User $usuarios)
    {
        // Dependencies automatically resolved by service container...
        $this->usuarios = $usuarios->all();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(['usuarios' => $this->usuarios]);
    }
}
