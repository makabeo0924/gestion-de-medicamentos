<?php


namespace App\Http\View\Composers;


use App\Bodega;
use App\User;
use Illuminate\View\View;

class BodegaComposer
{
    /**
     * The user repository implementation.
     *
     * @var User
     */
    protected $bodegas;

    protected $usuarios;

    /**
     * Create a new profile composer.
     *
     * @param Bodega $bodegas
     */
    public function __construct(Bodega $bodegas)
    {
        // Dependencies automatically resolved by service container...
        $this->bodegas = $bodegas->all();

        $this->usuarios = User::all();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(['bodegas' => $this->bodegas]);
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function composeCreate(View $view)
    {
        $view->with(['usuarios' => $this->usuarios]);
    }
}
