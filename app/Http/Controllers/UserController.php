<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        $usuario = new User();
        $usuario->name = $request->name;
        $usuario->cedula = $request->cedula;
        $usuario->password = bcrypt($request->password);
        $usuario->save();
        return redirect()->route('usuarios.index')->with('status', 'Usuario creado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $usuario)
    {
        return view('users.show', ['user' => $usuario]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $usuario)
    {
        return view('users.edit', ['user' => $usuario]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUser $request, User $usuario)
    {
        $usuario->name = $request->name;
        $usuario->cedula = $request->cedula;
        $usuario->save();
        return redirect()->route('usuarios.show', ['user' => $usuario])->with('status', 'Usuario actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(User $usuario)
    {
        if(isset($usuario->bodegas))
        {
            return redirect()->route('usuarios.index')->with('status', 'No se puede eliminar el usuario debido a que tiene una bodega asignada');
        }
        else {
            $usuario->delete();

            return redirect()->route('usuarios.index')->with('status', 'Usuario eliminado!');
        }
    }
}
