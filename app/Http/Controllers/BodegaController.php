<?php

namespace App\Http\Controllers;

use App\Bodega;
use App\Http\Requests\StoreBodega;
use App\User;
use Illuminate\Http\Request;

class BodegaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('bodegas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bodegas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBodega $request)
    {
        $bodega = new Bodega();
        $bodega->nombre = $request->nombre;
        $bodega->direccion = $request->direccion;

        $usuario = User::find($request->user_id);
        $bodega->usuario()->associate($usuario);

        $bodega->save();
        return redirect()->route('bodegas.index')->with('status', 'Bodega creada!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bodega  $bodega
     * @return \Illuminate\Http\Response
     */
    public function show(Bodega $bodega)
    {
        return view('bodegas.show', ['bodega' => $bodega]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bodega  $bodega
     * @return \Illuminate\Http\Response
     */
    public function edit(Bodega $bodega)
    {
        return view('bodegas.edit', ['bodega' => $bodega]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bodega  $bodega
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBodega $request, Bodega $bodega)
    {
        $bodega->nombre = $request->nombre;
        $bodega->direccion = $request->direccion;
        $usuario = User::find($request->user_id);
        $bodega->usuario()->associate($usuario);
        $bodega->save();
        return redirect()->route('bodegas.index')->with('status', 'Bodega actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Bodega $bodega
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Bodega $bodega)
    {
        $bodega->medicamentos()->detach();
        $bodega->delete();
        return redirect()->route('bodegas.index')->with('status', 'Bodega eliminada!');
    }
}
