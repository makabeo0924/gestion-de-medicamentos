<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePaciente;
use App\Paciente;
use Illuminate\Http\Request;

class PacienteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pacientes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pacientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePaciente $request)
    {
        $paciente = new Paciente();
        $paciente->nombre = $request->nombre;
        $paciente->residencia = $request->residencia;
        $paciente->cedula = $request->cedula;
        $paciente->save();
        return redirect()->route('pacientes.index')->with('status', 'Paciente creado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function show(Paciente $paciente)
    {
        return view('pacientes.show', ['paciente' => $paciente]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function edit(Paciente $paciente)
    {
        return view('pacientes.edit',['paciente'=>$paciente]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function update(StorePaciente $request, Paciente $paciente)
    {
        $paciente->nombre = $request->nombre;
        $paciente->residencia = $request->residencia;
        $paciente->cedula = $request->cedula;
        $paciente->save();
        return redirect()->route('pacientes.index')->with('status', 'Paciente actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Paciente $paciente
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Paciente $paciente)
    {
        $paciente->medicamentos()->detach();
        $paciente->delete();
        return redirect()->route('pacientes.index')->with('status', 'Paciente eliminado!');
    }
}
