<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMedicamento;
use App\Medicamento;
use Illuminate\Http\Request;

class MedicamentoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('medicamentos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('medicamentos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMedicamento $request)
    {
        $medicamento = new Medicamento();
        $medicamento->nombre = $request->nombre;
        $medicamento->descripcion = $request->descripcion;
        $medicamento->posologia = $request->posologia;
        $medicamento->save();
        return redirect()->route('medicamentos.index')->with('status', 'Medicamento creado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Medicamento  $medicamento
     * @return \Illuminate\Http\Response
     */
    public function show(Medicamento $medicamento)
    {
        return view('medicamentos.show', ['medicamento' => $medicamento]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Medicamento  $medicamento
     * @return \Illuminate\Http\Response
     */
    public function edit(Medicamento $medicamento)
    {
        return view('medicamentos.edit',['medicamento'=>$medicamento]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Medicamento  $medicamento
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMedicamento $request, Medicamento $medicamento)
    {
        $medicamento->nombre = $request->nombre;
        $medicamento->descripcion = $request->descripcion;
        $medicamento->posologia = $request->posologia;
        $medicamento->save();
        return redirect()->route('medicamentos.index')->with('status', 'Medicamento actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Medicamento $medicamento
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Medicamento $medicamento)
    {
        $medicamento->bodegas()->detach();
        $medicamento->pacientes()->detach();
        $medicamento->delete();
        return redirect()->route('medicamentos.index')->with('status', 'Medicamento eliminado!');
    }
}
