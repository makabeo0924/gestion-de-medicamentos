<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pacientes';

    protected $fillable = [
        'nombre', 'residencia', 'cedula',
    ];

    /**
     * The roles that belong to the user.
     */
    public function medicamentos()
    {
        return $this->belongsToMany(Medicamento::class, 'medicamentos_pacientes','paciente_id','medicamento_id')
            ->withPivot('cantidad');
    }
}
