@extends('layouts.app')

@section('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="../../adminlte/bower_components/select2/dist/css/select2.min.css">
@endsection

@section('content')
    @if(session()->has('status'))
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-info"></i>Mensaje</h4>
            {{ session()->get('status') }}
        </div>
    @endif

    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Paciente Nuevo</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <form role="form" method="POST" action="{{ route('pacientes.update',['paciente'=>$paciente]) }}">
                <input type="hidden" name="_method" value="put" />
                @csrf
                <div class="box-body">

                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input id="nombre" type="text" placeholder="Nombre del paciente" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ $paciente->nombre }}" required autocomplete="nombre" autofocus>
                        @error('nombre')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="residencia">Residencia</label>
                        <input id="residencia" type="text" placeholder="residencia" class="form-control @error('residencia') is-invalid @enderror" name="residencia" value="{{ $paciente->residencia }}" required autocomplete="residencia">
                        @error('residencia')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="cedula">Cedula</label>
                        <input id="cedula" type="text" placeholder="cedula" class="form-control @error('cedula') is-invalid @enderror" name="cedula" value="{{ $paciente->cedula }}" required autocomplete="cedula">
                        @error('cedula')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
    <!-- Select2 -->
    <script src="../../adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
    </script>
@endsection
