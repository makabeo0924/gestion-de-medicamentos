@extends('layouts.app')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="../../adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
    @if(session()->has('status'))
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-info"></i>Mensaje</h4>
            {{ session()->get('status') }}
        </div>
    @endif
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Pacientes</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="pacientes" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Cedula</th>
                    <th>Residencia</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($pacientes as $paciente)
                    <tr>
                        <td>{{$paciente->id}}</td>
                        <td>{{$paciente->nombre}}</td>
                        <td>{{$paciente->cedula}}</td>
                        <td>{{$paciente->residencia}}</td>
                        <td>
                            <a href="{{route('pacientes.show', ['paciente' => $paciente->id])}}" class="btn btn-info"><i class="fa fa-align-left"></i></a>
                            <a href="{{route('pacientes.edit', ['paciente' => $paciente->id])}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                            <form action="{{route('pacientes.destroy', ['paciente' => $paciente->id])}}" method="post" style="display: inline-block">
                                <input type="hidden" name="_method" value="delete" />
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-block btn-danger">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Cedula</th>
                    <th>Residencia</th>
                    <th>Acciones</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="../../adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#pacientes').DataTable();
        })
    </script>
@endsection
