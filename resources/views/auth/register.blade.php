@extends('auth.app')

@section('content')
    <form method="POST" action="{{ route('register') }}">
        @csrf


        <div class="form-group has-feedback">
                <input id="name" type="text" placeholder="Nombre Completo" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>


        <div class="form-group has-feedback">
            <input id="cedula" type="text" placeholder="Cedula" class="form-control @error('cedula') is-invalid @enderror" name="cedula" value="{{ old('cedula') }}" required autocomplete="cedula">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @error('cedula')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>


        <div class="form-group has-feedback">
                <input id="password" type="password" placeholder="Contraseña" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>



        <div class="form-group has-feedback">
                <input id="password-confirm" type="password" placeholder="Repite Contraseña" class="form-control" name="password_confirmation" required autocomplete="new-password">
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Register') }}
                </button>
            </div>
        </div>
    </form>
@endsection
