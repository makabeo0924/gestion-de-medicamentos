@extends('layouts.app')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="../../adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
    @if(session()->has('status'))
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-info"></i>Mensaje</h4>
            {{ session()->get('status') }}
        </div>
    @endif

    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Usuario</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
            <div class="box-body">
                <div class="form-group">
                    <label>Cedula</label>
                    <br>
                    <label>{{$user->cedula}}</label>
                </div>
                <div class="form-group">
                    <label >Nombre</label>
                    <br>
                    <label >{{$user->name}}</label>
                </div>
            </div>
            <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Bodegas a cargo</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="bodegas" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Direccion</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($user->bodegas as $bodega)
                    <tr>
                        <td>{{$bodega->id}}</td>
                        <td>{{$bodega->nombre}}</td>
                        <td>{{$bodega->direccion}}</td>
                        <td>
                            <a href="{{route('bodegas.show', ['bodega' => $bodega->id])}}" class="btn btn-info"><i class="fa fa-align-left"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Direccion</th>
                    <th>Acciones</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="../../adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#bodegas').DataTable();
        })
    </script>
@endsection
