@extends('layouts.app')

@section('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="../../adminlte/bower_components/select2/dist/css/select2.min.css">
@endsection

@section('content')
    @if(session()->has('status'))
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-info"></i>Mensaje</h4>
            {{ session()->get('status') }}
        </div>
    @endif

    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Medicamento Nuevo</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <form role="form" method="POST" action="{{ route('medicamentos.store') }}">
                @csrf
                <div class="box-body">

                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input id="nombre" type="text" placeholder="Nombre del medicamento" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>
                        @error('nombre')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripcion</label>
                        <input id="descripcion" type="text" placeholder="descripcion" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ old('descripcion') }}" required autocomplete="descripcion">
                        @error('descripcion')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="posologia">Posologia</label>
                        <input id="posologia" type="text" placeholder="posologia" class="form-control @error('posologia') is-invalid @enderror" name="posologia" value="{{ old('posologia') }}" required autocomplete="posologia">
                        @error('posologia')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
    <!-- Select2 -->
    <script src="../../adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
    </script>
@endsection
